#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" Voici mon module de test pour SimpleTestCalculator """


import unittest
import logging

from calculator.simplecalculator import SimpleCalculator as SimpleCalculator


class OperationTestSuite(unittest.TestCase):
    """ Voici ma classe de tests pour operation """

    def setUp(self):
        """ Executed before every test case """
        # self.calculator = SimpleCalculator()

    def test_operation_two_integers(self):
        """ Voici ma fonction de test pour une entree d un operande errone et de deux entiers """
        SimpleCalculator("Operation", 2, 3)
        result = SimpleCalculator("Operation", 2, 3).calcul()
        self.assertEqual(result, "Error")
        logging.warning("test_operation_two_integer ok warn")  # message ca marche
        logging.info(
            "test_operation_two_integer ok inf"
        )  # affiche si on active dans les options pytest


class AdditionTestSuite(unittest.TestCase):
    """ Voici ma classe de tests pour addition """

    def setUp(self):
        """ Executed before every test case """
        # self.calculator = SimpleCalculator()

    def test_addition_two_integers(self):
        """ Voici ma fonction de test pour une entree d un operande str et de deux entiers """
        SimpleCalculator("Somme", 2, 3)
        result = SimpleCalculator("Somme", 2, 3).calcul()
        self.assertEqual(result, 5)
        logging.warning("test_addition_two_integer ok warn")  # message ca marche
        logging.info(
            "test_addition_two_integer ok inf"
        )  # affiche si on active dans les options pytest

    def test_addtion_integer_string(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et d un str """
        SimpleCalculator("Somme", 2, "3")
        result = SimpleCalculator("Somme", 2, "3").calcul()
        self.assertEqual(result, "Error")
        logging.warning("test_addition_integer_string ok warn")  # message ca marche
        logging.info(
            "test_addition_integer_string ok inf"
        )  # affiche si on active dans les options pytest

    def test_addition_negative_integers(self):
        """ Voici ma fonction de test pour une entree d un operande, de deux entiers negatifs """
        SimpleCalculator("Somme", -2, -3)
        result = SimpleCalculator("Somme", -2, -3).calcul()
        self.assertEqual(result, -5)
        logging.warning("test_addition_negative_integers ok warn")  # message ca marche
        logging.info(
            "test_addition_negative_integers ok inf"
        )  # affiche si on active dans les options pytest

    def test_addition_integer_float(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et d un float """
        SimpleCalculator("Somme", 2, 3.3)
        result = SimpleCalculator("Somme", 2, 3.3).calcul()
        self.assertEqual(result, "Error")
        logging.warning("test_addition_integer_float ok warn")  # message ca marche
        logging.info(
            "test_addition_integer_float ok inf"
        )  # affiche si on active dans les options pytest

    def test_addition_integer_none(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et d un none """
        SimpleCalculator("Somme", 2, None)
        result = SimpleCalculator("Somme", 2, None).calcul()
        self.assertEqual(result, "Error")
        logging.warning("test_addition_integer_None ok warn")  # message ca marche
        logging.info(
            "test_addition_integer_None ok inf"
        )  # affiche si on active dans les options pytest

    def test_addition_integer_boolean(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et d un boolean """
        SimpleCalculator("Somme", 2, True)
        result = SimpleCalculator("Somme", 2, True).calcul()
        self.assertEqual(
            result, "Error"
        )  # On remarque que True est considere comme l integer 1
        logging.warning("test_addition_integer_boolean ok warn")  # message ca marche
        logging.info(
            "test_addition_integer_boolean ok inf"
        )  # affiche si on active dans les options pytest

    def test_addition_integer_booleanbis(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et d un boolean """
        SimpleCalculator("Somme", 2, False)
        result = SimpleCalculator("Somme", 2, False).calcul()
        self.assertEqual(
            result, "Error"
        )  # On remarque que True est considere comme l integer 0
        logging.warning("test_addition_integer_booleanbis ok warn")  # message ca marche
        logging.info(
            "test_addition_integer_booleanbis ok inf"
        )  # affiche si on active dans les options pytest

    def test_addition_integer_list(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et d une liste """
        SimpleCalculator("Somme", 2, [])
        result = SimpleCalculator("Somme", 2, []).calcul()
        self.assertEqual(result, "Error")
        logging.warning("test_addition_integer_list ok warn")  # message ca marche
        logging.info(
            "test_addition_integer_list ok inf"
        )  # affiche si on active dans les options pytest

    def test_addition_integer_dictionary(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et dictionnaire """
        SimpleCalculator("Somme", 2, {})
        result = SimpleCalculator("Somme", 2, {}).calcul()
        self.assertEqual(result, "Error")
        logging.warning("test_addition_integer_dictionary ok warn")  # message ca marche
        logging.info(
            "test_addition_integer_dictionary ok inf"
        )  # affiche si on active dans les options pytest


class DifferenceTestSuite(unittest.TestCase):
    """ Voici ma classe de tests pour difference """

    def setUp(self):
        """ Executed before every test case """
        # self.calculator = SimpleCalculator()

    def test_difference_two_integers(self):
        """ Voici ma fonction de test pour une entree d un operande str et de deux entiers """
        SimpleCalculator("Difference", 2, 3)
        result = SimpleCalculator("Difference", 2, 3).calcul()
        self.assertEqual(result, -1)
        logging.warning("test_difference_two_integer ok warn")  # message ca marche
        logging.info(
            "test_difference_two_integer ok inf"
        )  # affiche si on active dans les options pytest

    def test_addtion_integer_string(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et d un str """
        SimpleCalculator("Difference", 2, "3")
        result = SimpleCalculator("Difference", 2, "3").calcul()
        self.assertEqual(result, "Error")
        logging.warning("test_difference_integer_string ok warn")  # message ca marche
        logging.info(
            "test_difference_integer_string ok inf"
        )  # affiche si on active dans les options pytest

    def test_difference_negative_integers(self):
        """ Voici ma fonction de test pour une entree d un operande, de deux entiers negatifs """
        SimpleCalculator("Difference", -2, -3)
        result = SimpleCalculator("Difference", -2, -3).calcul()
        self.assertEqual(result, 1)
        logging.warning(
            "test_difference_negative_integers ok warn"
        )  # message ca marche
        logging.info(
            "test_difference_negative_integers ok inf"
        )  # affiche si on active dans les options pytest

    def test_difference_integer_float(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et d un float """
        SimpleCalculator("Difference", 2, 3.3)
        result = SimpleCalculator("Difference", 2, 3.3).calcul()
        self.assertEqual(result, "Error")
        logging.warning("test_difference_integer_float ok warn")  # message ca marche
        logging.info(
            "test_difference_integer_float ok inf"
        )  # affiche si on active dans les options pytest

    def test_difference_integer_none(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et d un none """
        SimpleCalculator("Difference", 2, None)
        result = SimpleCalculator("Difference", 2, None).calcul()
        self.assertEqual(result, "Error")
        logging.warning("test_difference_integer_None ok warn")  # message ca marche
        logging.info(
            "test_difference_integer_None ok inf"
        )  # affiche si on active dans les options pytest

    def test_difference_integer_boolean(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et d un boolean """
        SimpleCalculator("Difference", 2, True)
        result = SimpleCalculator("Difference", 2, True).calcul()
        self.assertEqual(
            result, "Error"
        )  # On remarque que True est considere comme l integer 1
        logging.warning("test_difference_integer_boolean ok warn")  # message ca marche
        logging.info(
            "test_difference_integer_boolean ok inf"
        )  # affiche si on active dans les options pytest

    def test_difference_integer_booleanbis(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et d un boolean """
        SimpleCalculator("Difference", 2, False)
        result = SimpleCalculator("Difference", 2, False).calcul()
        self.assertEqual(
            result, "Error"
        )  # On remarque que True est considere comme l integer 0
        logging.warning(
            "test_difference_integer_booleanbis ok warn"
        )  # message ca marche
        logging.info(
            "test_difference_integer_booleanbis ok inf"
        )  # affiche si on active dans les options pytest

    def test_difference_integer_list(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et d une liste """
        SimpleCalculator("Difference", 2, [])
        result = SimpleCalculator("Difference", 2, []).calcul()
        self.assertEqual(result, "Error")
        logging.warning("test_difference_integer_list ok warn")  # message ca marche
        logging.info(
            "test_difference_integer_list ok inf"
        )  # affiche si on active dans les options pytest

    def test_difference_integer_dictionary(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et dictionnaire """
        SimpleCalculator("Difference", 2, {})
        result = SimpleCalculator("Difference", 2, {}).calcul()
        self.assertEqual(result, "Error")
        logging.warning(
            "test_difference_integer_dictionary ok warn"
        )  # message ca marche
        logging.info(
            "test_difference_integer_dictionary ok inf"
        )  # affiche si on active dans les options pytest


class ProduitTestSuite(unittest.TestCase):
    """ Voici ma classe de tests pour produit """

    def setUp(self):
        """ Executed before every test case """
        # self.calculator = SimpleCalculator()

    def test_produit_two_integers(self):
        """ Voici ma fonction de test pour une entree d un operande str et de deux entiers """
        SimpleCalculator("Produit", 2, 3)
        result = SimpleCalculator("Produit", 2, 3).calcul()
        self.assertEqual(result, 6)
        logging.warning("test_produit_two_integer ok warn")  # message ca marche
        logging.info(
            "test_produit_two_integer ok inf"
        )  # affiche si on active dans les options pytest

    def test_addtion_integer_string(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et d un str """
        SimpleCalculator("Produit", 2, "3")
        result = SimpleCalculator("Produit", 2, "3").calcul()
        self.assertEqual(result, "Error")
        logging.warning("test_produit_integer_string ok warn")  # message ca marche
        logging.info(
            "test_produit_integer_string ok inf"
        )  # affiche si on active dans les options pytest

    def test_produit_negative_integers(self):
        """ Voici ma fonction de test pour une entree d un operande, de deux entiers negatifs """
        SimpleCalculator("Produit", -2, -3)
        result = SimpleCalculator("Produit", -2, -3).calcul()
        self.assertEqual(result, 6)
        logging.warning("test_produit_negative_integers ok warn")  # message ca marche
        logging.info(
            "test_produit_negative_integers ok inf"
        )  # affiche si on active dans les options pytest

    def test_produit_integer_float(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et d un float """
        SimpleCalculator("Produit", 2, 3.3)
        result = SimpleCalculator("Produit", 2, 3.3).calcul()
        self.assertEqual(result, "Error")
        logging.warning("test_produit_integer_float ok warn")  # message ca marche
        logging.info(
            "test_produit_integer_float ok inf"
        )  # affiche si on active dans les options pytest

    def test_produit_integer_none(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et d un none """
        SimpleCalculator("Produit", 2, None)
        result = SimpleCalculator("Produit", 2, None).calcul()
        self.assertEqual(result, "Error")
        logging.warning("test_produit_integer_None ok warn")  # message ca marche
        logging.info(
            "test_produit_integer_None ok inf"
        )  # affiche si on active dans les options pytest

    def test_produit_integer_boolean(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et d un boolean """
        SimpleCalculator("Produit", 2, True)
        result = SimpleCalculator("Produit", 2, True).calcul()
        self.assertEqual(
            result, "Error"
        )  # On remarque que True est considere comme l integer 1
        logging.warning("test_produit_integer_boolean ok warn")  # message ca marche
        logging.info(
            "test_produit_integer_boolean ok inf"
        )  # affiche si on active dans les options pytest

    def test_produit_integer_booleanbis(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et d un boolean """
        SimpleCalculator("Produit", 2, False)
        result = SimpleCalculator("Produit", 2, False).calcul()
        self.assertEqual(
            result, "Error"
        )  # On remarque que True est considere comme l integer 0
        logging.warning("test_produit_integer_booleanbis ok warn")  # message ca marche
        logging.info(
            "test_produit_integer_booleanbis ok inf"
        )  # affiche si on active dans les options pytest

    def test_produit_integer_list(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et d une liste """
        SimpleCalculator("Produit", 2, [])
        result = SimpleCalculator("Produit", 2, []).calcul()
        self.assertEqual(result, "Error")
        logging.warning("test_produit_integer_list ok warn")  # message ca marche
        logging.info(
            "test_produit_integer_list ok inf"
        )  # affiche si on active dans les options pytest

    def test_produit_integer_dictionary(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et dictionnaire """
        SimpleCalculator("Produit", 2, {})
        result = SimpleCalculator("Produit", 2, {}).calcul()
        self.assertEqual(result, "Error")
        logging.warning("test_produit_integer_dictionary ok warn")  # message ca marche
        logging.info(
            "test_produit_integer_dictionary ok inf"
        )  # affiche si on active dans les options pytest


class DivisionTestSuite(unittest.TestCase):
    """ Voici ma classe de tests pour division """

    def setUp(self):
        """ Executed before every test case """
        # self.calculator = SimpleCalculator()

    def test_division_two_integers(self):
        """ Voici ma fonction de test pour une entree d un operande str et de deux entiers """
        SimpleCalculator("Division", 2, 3)
        result = SimpleCalculator("Division", 2, 3).calcul()
        self.assertEqual(result, 2 / 3)
        logging.warning("test_division_two_integer ok warn")  # message ca marche
        logging.info(
            "test_division_two_integer ok inf"
        )  # affiche si on active dans les options pytest

    def test_addtion_integer_string(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et d un str """
        SimpleCalculator("Division", 2, "3")
        result = SimpleCalculator("Division", 2, "3").calcul()
        self.assertEqual(result, "Error")
        logging.warning("test_division_integer_string ok warn")  # message ca marche
        logging.info(
            "test_division_integer_string ok inf"
        )  # affiche si on active dans les options pytest

    def test_division_negative_integers(self):
        """ Voici ma fonction de test pour une entree d un operande, de deux entiers negatifs """
        SimpleCalculator("Division", -2, -3)
        result = SimpleCalculator("Division", -2, -3).calcul()
        self.assertEqual(result, 2 / 3)
        logging.warning("test_division_negative_integers ok warn")  # message ca marche
        logging.info(
            "test_division_negative_integers ok inf"
        )  # affiche si on active dans les options pytest

    def test_division_integer_float(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et d un float """
        SimpleCalculator("Division", 2, 3.3)
        result = SimpleCalculator("Division", 2, 3.3).calcul()
        self.assertEqual(result, "Error")
        logging.warning("test_division_integer_float ok warn")  # message ca marche
        logging.info(
            "test_division_integer_float ok inf"
        )  # affiche si on active dans les options pytest

    def test_division_integer_one(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et d un none """
        SimpleCalculator("Division", 2, None)
        result = SimpleCalculator("Division", 2, None).calcul()
        self.assertEqual(result, "Error")
        logging.warning("test_division_integer_None ok warn")  # message ca marche
        logging.info(
            "test_division_integer_None ok inf"
        )  # affiche si on active dans les options pytest

    def test_division_integer_boolean(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et d un boolean """
        SimpleCalculator("Division", 2, True)
        result = SimpleCalculator("Division", 2, True).calcul()
        self.assertEqual(
            result, "Error"
        )  # On remarque que True est considere comme l integer 1
        logging.warning("test_division_integer_boolean ok warn")  # message ca marche
        logging.info(
            "test_division_integer_boolean ok inf"
        )  # affiche si on active dans les options pytest

    def test_division_integer_booleanbis(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et d un boolean """
        SimpleCalculator("Division", 2, False)
        result = SimpleCalculator("Division", 2, False).calcul()
        self.assertEqual(
            result, "Error"
        )  # On remarque que True est considere comme l integer 0
        logging.warning("test_division_integer_booleanbis ok warn")  # message ca marche
        logging.info(
            "test_division_integer_booleanbis ok inf"
        )  # affiche si on active dans les options pytest

    def test_division_integer_list(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et d une liste """
        SimpleCalculator("Division", 2, [])
        result = SimpleCalculator("Division", 2, []).calcul()
        self.assertEqual(result, "Error")
        logging.warning("test_division_integer_list ok warn")  # message ca marche
        logging.info(
            "test_division_integer_list ok inf"
        )  # affiche si on active dans les options pytest

    def test_division_integer_dictionary(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et dictionnaire """
        SimpleCalculator("Division", 2, {})
        result = SimpleCalculator("Division", 2, {}).calcul()
        self.assertEqual(result, "Error")
        logging.warning("test_division_integer_dictionary ok warn")  # message ca marche
        logging.info(
            "test_division_integer_dictionary ok inf"
        )  # affiche si on active dans les options pytest

    def test_division_integer_zero(self):
        """ Voici ma fonction de test pour une entree d un operande, d un entier et zero """
        SimpleCalculator("Division", 2, 0)
        result = SimpleCalculator("Division", 2, 0).calcul()
        self.assertEqual(result, "Error")
        logging.warning("test_division_integer_zero ok warn")  # message ca marche
        logging.info(
            "test_division_integer_zero ok inf"
        )  # affiche si on active dans les options pytest


if __name__ == "__main__":
    unittest.main()
