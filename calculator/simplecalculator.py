#!/usr/bin/env python
# coding: utf-8
"""
Ce code fonctionne
"""


class SimpleCalculator:
    """ Ma classe de calcul """

    # @staticmethod
    def __init__(self, var_nom_de_fonction, var_a, var_b):
        # Perte de score car l interet n est pas a l affectation
        self.var_nom = var_nom_de_fonction
        self.var_x = var_a
        self.var_y = var_b

    def calcul(self):
        """ Fonction principale de calcul """
        if isinstance(self.var_nom, str):
            if isinstance(self.var_x, int) and isinstance(self.var_y, int):
                # Double if sinon je perds plus de score a cause de ligne trop longue
                if self.var_nom == "Somme":
                    res = self.var_x + self.var_y
                elif self.var_nom == "Difference":
                    res = self.var_x - self.var_y
                elif self.var_nom == "Produit":
                    res = self.var_x * self.var_y
                elif self.var_nom == "Division" and self.var_y != 0:
                    res = self.var_x / self.var_y
                elif self.var_nom == "Division" and self.var_y == 0:
                    res = "Error"
                    # raise ZeroDivisionError("Cannot divide by zero")
                    # return "Error"
                else:
                    res = "Error"
                    # print(self.var_x, " ", self.var_nom, " ", self.var_y, " = ", res, ", else1")
            else:
                res = "Error"
                # print(self.var_x, " ", self.var_nom, " ", self.var_y, " = ", res, ", else2")
            # raise = TypeError("wrong type")
        # Cas ou on ait un type autre qu integer pour les deux variables
        # return "Error"
        else:
            res = "Error"
            # print(self.var_x, " ", self.var_nom, " ", self.var_y, " = ", res, ", else2")
        if self.var_x == True or self.var_x == False:
            # Je suis oblige de laisser True et False au lieu de if X or nor Y donc black rouspete
            res = "Error"
        if self.var_y == True or self.var_y == False:
            res = "Error"
        # print(self.var_x, " ", self.var_nom, " ", self.var_y, " = ", res, ", SANS ELSE")
        return res
        # raise TypeError("wrong type")


# Cas ou on ait un type autre que str pour le choix du calcul a effectuer
# return "Error"


###############################################
if __name__ == "__main__":
    VAL_A = int(input("Veuillez rentrer a entier... "))
    VAL_B = int(input("veuillez rentrer b entier... "))

    SimpleCalculator("Somme", VAL_A, VAL_B)
    print(
        "Résultat de la somme : a + b = ",
        SimpleCalculator("Somme", VAL_A, VAL_B).calcul(),
    )

    SimpleCalculator("Difference", VAL_A, VAL_B)
    print(
        "Résultat de la somme : a - b = ",
        SimpleCalculator("Difference", VAL_A, VAL_B).calcul(),
    )

    SimpleCalculator("Produit", VAL_A, VAL_B)
    print(
        "Résultat de la somme : a * b = ",
        SimpleCalculator("Produit", VAL_A, VAL_B).calcul(),
    )

    SimpleCalculator("Division", VAL_A, VAL_B)
    print(
        "Résultat de la somme : a / b = ",
        SimpleCalculator("Division", VAL_A, VAL_B).calcul(),
    )
